angular.module('LoginCtrl',[])

.controller('LoginCtrl',function($scope){
    $scope.loginSubmit = function(){
        console.dir($scope.loginData);
    }
});

angular.module('movieApp.controllers',['ImageCropper'])

.controller('MovieListController',['$scope', '$http', '$state', 'popupService', '$window', 'Movie',
    function($scope,$http,$state,popupService,$window,Movie){
    $scope.movies=Movie.query();
}])

.controller('MovieViewController',['$scope', '$http', '$state', '$stateParams', 'popupService', '$window', 'Movie',
    function($scope,$http,$state,$stateParams,popupService,$window,Movie){    
    $scope.movie=Movie.get({id:$stateParams.id});
    $scope.deleteMovie=function(id){
        if(popupService.showPopup('Really delete this?')){
            $http.delete('/movies/' + id)
                .success(function(data, status, headers, config){
                    $window.location.href='';
                })
                .error(function(){
                    console.log(data, status, headers, config)
                });
        }
    }
}])

.controller('MovieCreateController', ['$scope', '$http', '$state', '$stateParams', 'Movie', 'Actor',
        function($scope,$http,$state,$stateParams,Movie,Actor){
    $scope.movie=new Movie();

    $scope.$on('imageCropped', function(event, data){
        $scope.movie.image = data;
    });

    $scope.$on('actorImageCropped', function(event, data){
        $scope.movie.actorimage = data;
    });

    $scope.movie.actorlist = Actor.query();

    var actorlist = $scope.movie.actorlist;

    $scope.movie.cast = [];

    var cast = $scope.movie.cast;

    $scope.addActor = function(){
        $http.post('/actors', {
            name: $scope.movie.actorname,
            image: $scope.movie.actorimage
        })
            .success(function(data, status, headers, config){
                if(data.error == true){
                    $scope.errors = data.description;
                }else {
                    var array = $scope.movie.actorlist;
                    array.push(data);
                    $scope.movie.actorname = '';
                    $scope.$broadcast('actorImageCompleted');
                }
            })
            .error(function(data, status, headers, config){
                console.log(data.description);
            });
    };

    $scope.deleteActor = function(array, currentPage, perPage, index, id){
        $http.delete('/actors/' + id)
            .success(function(data, status, headers, config){
                index = index + (((Number(currentPage) - 1) * Number(perPage)));
                array.splice(index, 1);
                console.log(data);
            })
            .error(function(data, status, headers, config){
                console.log(data);
            });
    };

    $scope.addCast = function(actor, array, index){
        cast.push(actor);
        array.splice(index, 1);
    };

    $scope.removeCast = function(actor, array, index){
        actorlist.push(actor);
        array.splice(index, 1);
    };

    $scope.addMovie=function(){
        $http.post('/movies', $scope.movie)
            .success(function(data, status, headers, config){
                if(data.error == true){
                    $scope.errors = data.description;
                }else{
                    var loc = window.location;
                    var hash = 'movies';
                    loc.hash = hash;
                    window.location = loc;
                }
            })
            .error(function(data, status, headers, config){
                console.log(data);
            });    }

}])

.controller('MovieEditController',['$scope', '$http', '$state', '$stateParams', 'Movie', 'Actor',
        function($scope,$http,$state,$stateParams,Movie,Actor){
    var id = $stateParams.id;
    $scope.$on('imageCropped', function(event, data){
        $scope.movie.image = data;
    });
    $scope.$on('actorImageCropped', function(event, data){
        $scope.movie.actorimage = data;
    });
    var load = $scope.movie=Movie.get({id:$stateParams.id});
    load.$promise.then(function(data){
        $scope.movie.actorlist = Actor.query();
        var actorlist = $scope.movie.actorlist;
        actorlist.$promise.then(function(data){

            if(typeof $scope.movie.cast === 'undefined'){
                $scope.movie.cast = [];
            }
            var cast = $scope.movie.cast;
            angular.forEach(cast, function(actor, key){
                var id = actor.id;
                var idx = actorlist.map(function(obj, index){
                    if(obj.id == id){
                        return index;
                    }
                }).filter(isFinite);
                actorlist.splice(idx, 1);
            });
            $scope.addActor = function(){
                $http.post('/actors', {
                    name: $scope.movie.actorname,
                    image: $scope.movie.actorimage
                })
                    .success(function(data, status, headers, config){
                        if(data.error == true){
                            $scope.errors = data.description;
                        }else {
                            var array = $scope.movie.actorlist;
                            array.push(data);
                            $scope.movie.actorname = '';
                            $scope.$broadcast('actorImageCompleted');
                        }
                    })
                    .error(function(data, status, headers, config){
                        console.log(data.description);
                    });
            };

            $scope.deleteActor = function(array, currentPage, perPage, index, id){
                $http.delete('/actors/' + id)
                    .success(function(data, status, headers, config){
                        index = index + (((Number(currentPage) - 1) * Number(perPage)));
                        array.splice(index, 1);
                        console.log(data);
                    })
                    .error(function(data, status, headers, config){
                        console.log(data);
                    });
            };

            $scope.addCast = function(actor, array, index){
                cast.push(actor);
                array.splice(index, 1);
            };

            $scope.removeCast = function(actor, array, index){
                actorlist.push(actor);
                array.splice(index, 1);
            };

            $scope.updateMovie=function(){
                $http.put('/movies/' + $stateParams.id, $scope.movie)
                    .success(function(data, status, headers, config){
                        if(data.error == true){
                            $scope.errors = data.description;
                        }else{
                            var loc = window.location;
                            var hash = '#/movies/' + id + '/view';
                            loc.hash = hash;
                            window.location = loc.href;
                        }
                    })
                    .error(function(data, status, headers, config){
                        console.log(data);
                    });
            };
        });
    });
}]);

function PaginationCtrl($scope) {

    $scope.currentPage = 1;
    $scope.pageSize = 10;

    $scope.pageChangeHandler = function(num) {
        console.log('movies page changed to ' + num);
    };
}

function PaginationControlsCtrl($scope) {
    $scope.pageChangeHandler = function(num) {
        console.log('going to page ' + num);
    };
}

function ImageCtrl($scope){

    $scope.imageCropResult = null;
    $scope.actorImageCropResult = null;
    $scope.showImageCropper = false;
    $scope.showActorImageCropper = false;

    $scope.$watch('imageCropResult', function(){
        $scope.$emit('imageCropped', $scope.imageCropResult);
    });

    $scope.$watch('actorImageCropResult', function(){
        $scope.$emit('actorImageCropped', $scope.actorImageCropResult);
    });

    $scope.$on('actorImageCompleted', function(event, data){
        $scope.actorImageCropResult = null;
        $scope.showActorImageCropper = false;
    });

}

movieApp.controller('PaginationCtrl', PaginationCtrl);
movieApp.controller('PaginationControlsCtrl', PaginationControlsCtrl);
movieApp.controller('ImageCtrl', ImageCtrl);