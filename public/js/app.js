var movieApp = angular.module('movieApp',[
    'ui.router',
    'ngResource',
    'movieApp.controllers',
    'movieApp.services',
    'angularUtils.directives.dirPagination',
    'ImageCropper'
]);

angular.module('movieApp').config(function($stateProvider,$httpProvider){
    $stateProvider.state('movies',{
        url:'/movies',
        templateUrl:'/partials/movies.html',
        controller:'MovieListController'
    }).state('viewMovie',{
       url:'/movies/:id/view',
       templateUrl:'/partials/movie-view.html',
       controller:'MovieViewController'
    }).state('newMovie',{
        url:'/movies/new',
        templateUrl:'/partials/movie-add.html',
        controller:'MovieCreateController'
    }).state('editMovie', {
        url: '/movies/:id/edit',
        templateUrl: '/partials/movie-edit.html',
        controller: 'MovieEditController'
    }).state('login', {
        url: '/',
        templateUrl: '/partials/login.html',
        controller: 'loginCtrl'
    });
}).run(function($state){
   $state.go('movies');
});