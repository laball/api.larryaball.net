var app = angular.module('movieApp.services',[]);
app.factory('Movie',function($resource) {
    return $resource('/movies/:id', {id: '@id'}, {
        update: {
            method: 'PUT'
        }
    });
});
app.factory('Actor',function($resource) {
    return $resource('/actors/:id', {id: '@id'}, {

    });
});
app.service('popupService',function($window){
    this.showPopup=function(message){
        return $window.confirm(message);
    };
});