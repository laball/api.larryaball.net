<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::group(array('before' => 'auth'), function() {
    Route::get('/', array('as' => 'home', 'uses' => 'HomeController@getIndex'));

    Route::get('/actors', array('uses' =>
        'ActorController@getActors'));
    Route::get('/movies', array('uses' =>
        'MovieController@getMovies'));
    Route::get('/actors/{id}', array('uses' =>
        'ActorController@getActorInfo'));
    Route::get('/movies/{id}', array('uses' =>
        'MovieController@getMovieInfo'));
    Route::post('/actors', array('uses' =>
        'ActorController@postActor'));
    Route::post('/movies', array('uses' =>
        'MovieController@postMovie'));
    Route::put('/actors/{id}', array('uses' =>
        'ActorController@putActor'));
    Route::put('/movies/{id}', array('uses' =>
        'MovieController@putMovie'));
    Route::delete('/actors/{id}', array('uses' =>
        'ActorController@deleteActor'));
    Route::delete('/movies/{id}', array('uses' =>
        'MovieController@deleteMovie'));
});

Route::controller('users', 'UserController');