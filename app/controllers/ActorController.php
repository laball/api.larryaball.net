<?php
use MovieApp\Service\Validation\ActorValidator;

class ActorController extends \BaseController {
    protected $validator;

    public function __construct(ActorValidator $validator){
        $this->validator = $validator;
    }
    public function getActors(){
        $actors = Actor::all();

        return Response::json($actors);
    }
    public function getActorInfo($id){
        $actor = Actor::findOrFail($id);
        if($actor){
            $actorInfo = array(
                'error' => false,
                'name' => $actor->name,
                'id' => $actor->id
            );
            $actormovies = json_decode($actor->Movies);
            foreach($actormovies as $movie){
                $movielist[] = array(
                    "name" => $movie->name,
                    "release_year" => $movie->release_year
                );
            }
            $movielist = array('Movies' => $movielist);
            return Response::json(array_merge($actorInfo, $movielist));
        }else{
            return Response::json(array(
                'error' => true,
                'description' => 'We could not find any actor in the database like: '.$actorname
            ));
        }
    }

    public function postActor(){
        $actor = Input::get();
        if(!$this->validator->with($actor)->passes()){
            return Response::json(array(
                'error' => true,
                'description' => $this->validator->errors()
            ));
        }
        $the_actor = Actor::create(array(
            'name' => Input::get('name'),
            'image' => Input::get('image')
            )
        );
        return Response::json(array(
            'error'=>false,
            'description'=>'The actor successfully saved.',
            'name' => $the_actor->name,
            'id' => $the_actor->id
        ));
    }

    public function putActor($id){
        $input = Input::get();
        if(!$this->validator->with($input)->passes()){
            return Response::json(array(
                'error' => true,
                'description' => $this->validator->errors()
            ));
        }
        $actorname = $input->name;
        $actor = Actor::findorfail($id);
        $base64string = Input::get('image') ? Input::get('image') : false;
        if($base64string) {
            $actor->update(array(
                'name' => Input::get('name', $actor->name),
                'image' => $base64string
            ));
        }else{
            $actor->update(array(
                'name' => Input::get('name', $actor->name)
            ));
        }
        $the_actor = Actor::create(array(
            'name' => Input::get('name'),
            'image' => Input::get('image')
            )
        );
        return Response::json(array(
            'error'=>false,
            'description'=>'The actor successfully saved.',
            'name' => $the_actor->name,
            'id' => $the_actor->id
        ));
    }

    public function deleteActor($id){
        $actor = Actor::find($id);
        if ($actor) {
            $actor->delete();
            $affectedRows = DB::table('pivot_table')->where('actor_id', '=', $id)->delete();

            return Response::json(array(
                'error' => false,
                'description' => 'The actor successfully deleted: ' . $actor->name,
                'stats' => $affectedRows . ' deleted from api.pivot_table'
            ));
        } else {
            return Response::json(array(
                'error' => true,
                'description' => 'We could not find any actor in the database with ID number: ' . $id
            ));
        }
    }

}
