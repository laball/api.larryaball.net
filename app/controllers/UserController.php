<?php
/**
 * Created by PhpStorm.
 * User: lball
 * Date: 1/25/15
 * Time: 9:40 PM
 */
use MovieApp\Service\Validation\UserValidator;
class UserController extends \BaseController{
    protected $layout = '_layouts.main';
    protected $validator;

    public function __construct(UserValidator $validator){
        $this->validator = $validator;
        $this->beforeFilter('csrf', array('on'=>'post')); //pass CSRF token on form submission
    }
    public function getRegister(){
        $this->layout->content = View::make('users.register');
    }
    public function postCreate(){
        $user = Input::get();
        if(!$this->validator->with($user)->passes()){
            return Redirect::back()
                ->with('message', 'The following errors occurred')
                ->withErrors($this->validator->errors())
                ->withInput();
        }else{
            $the_user = new User;
            $the_user->email = Input::get('email');
            $the_user->password = Hash::make(Input::get('password'));
            $the_user->save();
            return Redirect::to('users/login')->with(array(
                'message' => 'Thank you for registering.',
                'message-type' => 'info'
            ));
        }
    }
    public function getLogin(){
        $this->layout->content = View::make('users.login');
    }
    public function postSignin(){
        if (Auth::attempt(array('email'=>Input::get('email'), 'password'=>Input::get('password')))) {
            return Redirect::intended('/');
        } else {
            return Redirect::to('users/login')
                ->with(array(
                    'message' => 'Your username/password combination was incorrect',
                    'message-type' => 'warning'
                    ))
                ->withInput();
        }
    }
    public function getLogout(){
        Auth::logout();
        return Redirect::to('users/login')->with(array(
            'message' => 'You are now logged out.',
            'message-type' => 'info'
            ));
    }
}