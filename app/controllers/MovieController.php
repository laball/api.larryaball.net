<?php
use MovieApp\Service\Validation\MovieValidator;

class MovieController extends \BaseController {
    protected $validator;

    public function __construct(MovieValidator $validator){
        $this->validator = $validator;
    }
    public function getMovies(){
        $movies = Movie::all();

        return Response::json($movies);
    }
    public function getMovieInfo($id){
        $movie = Movie::findOrFail($id);
        if($movie){
            $movieInfo = array(
                'error' => false,
                'name' => $movie->name,
                'release_year' => $movie->release_year,
                'id' => $movie->id,
                'image' => $movie->image
            );
            $movieactors = json_decode($movie->Actors);
            foreach($movieactors as $actor){
                $actorlist[] = array(
                    'id' => $actor->id,
                    'name' => $actor->name,
                    'image' => $actor->image
                );
            }
            if(isset($actorlist)){
                $actorlist = array('cast' => $actorlist);
                return Response::json(array_merge($movieInfo, $actorlist));
            }else{
                return Response::json($movieInfo);
            }
        }
    }

    public function putMovie($id){
        $input = Input::get();
        if(!$this->validator->with($input)->passes()){
            return Response::json(array(
                'error' => true,
                'description' => $this->validator->errors()
            ));
        }
        $movie = Movie::findOrFail($id);
        $base64string = Input::get('image') ? Input::get('image') : false;
        if($base64string) {
            $movie->update(array(
                'name' => Input::get('name', $movie->name),
                'release_year' => Input::get('release_year', $movie->release_year),
                'image' => $base64string
            ));
        }else{
            $movie->update(array(
                'name' => Input::get('name', $movie->name),
                'release_year' => Input::get('release_year', $movie->release_year)
            ));
        }
        $newcast = Input::get('cast');
        $oldcast = DB::table('pivot_table')->where('movie_id', '=', $id)->delete();

        foreach ($newcast as $actor) {
            DB::table('pivot_table')->insert(
                array('movie_id' => $id,
                    'actor_id' => $actor['id']
                )
            );
        }
        return Response::json(array(
            'error' => false,
            'description' => 'The movie successfully saved. The ID number of Movie is: ' . $movie->id
                . '. The name of the movie is ' . $movie->name . '. The movie\'s year of release is '
                . $movie->release_year . '.'
        ));
    }
    public function postMovie(){
        $input = Input::get();
        array_shift($this->validator->rules);
        $this->validator->rules['name'] = 'required|unique:movies,name';
        if(!$this->validator->with($input)->passes()){
            return Response::json(array(
               'error' => true,
                'description' => $this->validator->errors()
            ));
        }

        $the_movie = Movie::create(array(
            'name' => Input::get('name'),
            'release_year' => Input::get('release_year'),
            'image' => Input::get('image')
        ));
        $cast = Input::get('cast');
        foreach($cast as $actor){
            DB::table('pivot_table')->insert(
                array('movie_id' => $the_movie->id,
                      'actor_id' => $actor['id']
                )
            );
        }
        return Response::json(array(
            'error'=>false,
            'description'=>'The movie successfully saved. The ID number of Movie is: '.$the_movie->id
        ));
    }

    public function deleteMovie($id){
        $movie = Movie::find($id);
        if ($movie) {
            $movie->delete();
            $affectedRows = DB::table('pivot_table')->where('movie_id', '=', $id)->delete();
            return Response::json(array(
                'error' => false,
                'description' => 'The movie successfully deleted: ' . $movie->name,
                'stats' => $affectedRows . ' deleted from api.pivot_table'
            ));
        } else {
            return Response::json(array(
                'error' => true,
                'description' => 'We could not find any movie in the database with ID number: ' . $id
            ));
        }
    }
}