<?php
/**
 * Created by PhpStorm.
 * User: lball
 * Date: 1/18/15
 * Time: 4:49 AM
 */

class ValidationException extends \Exception{

    /**
     * Errors object.
     *
     * @var
     */
    protected $validator;

    /**
     * Create a new validate exception instance.
     *
     * @param  $container
     */
    public function __construct(Validator $validator){
        $this->message = 'Validation has failed.';
        $this->validator = $validator;
    }

    /**
     * Gets the errors object.
     *
     * @return
     */
    public function getErrors(){
        return $this->validator->messages();
    }

}