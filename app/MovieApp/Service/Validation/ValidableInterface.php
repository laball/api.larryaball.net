<?php
/**
 * Created by PhpStorm.
 * User: lball
 * Date: 1/21/15
 * Time: 5:10 PM
 */

namespace MovieApp\Service\Validation;


interface ValidableInterface {

    /**
     * Add data to validate against
     *
     * @param array
     * @return \MovieApp\Service\Validation\ValidableInterface
     */
    public function with(array $input);

    /**
     * Test if validation passes
     *
     * @return boolean
     */
    public function passes();

    /**
     * Retrieve validation errors
     *
     * @return array
     */
    public function errors();
}