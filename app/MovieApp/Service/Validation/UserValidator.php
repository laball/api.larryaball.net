<?php
/**
 * Created by PhpStorm.
 * User: lball
 * Date: 1/25/15
 * Time: 11:51 PM
 */

namespace MovieApp\Service\Validation;


class UserValidator extends AbstractLaravelValidator{

    /**
     * Validation rules
     *
     * @var Array
     *
     */
    public $rules = array(
        'email' => 'required|email|unique:users',
        'password' => 'required|alpha_num|between:6,12|confirmed',
        'password_confirmation' => 'required|alpha_num|between:6,12'
    );
    /**
     * Validation messages
     *
     * @var Array
     */
    protected $messages = array(
        'email.unique' => 'That email address is already registered.'
    );
}