<?php
/**
 * Created by PhpStorm.
 * User: lball
 * Date: 1/18/15
 * Time: 4:44 AM
 */
namespace MovieApp\Service\Validation;

use Illuminate\Validation\Factory as Validator;

abstract class AbstractLaravelValidator implements ValidableInterface{

    /**
     * Validator
     *
     * @var \Illuminate\Validation\Factory
     */
    protected $validator;

    /**
     * Validation data key => value array.
     *
     * @var Array
     */
    protected $data = [];

    /**
     * Validation errors
     *
     * @var Array
     */
    protected $errors = [];

    /**
     * Array of validation rules.
     *
     * @var Array
     */
    protected $rules = [];

    /**
     * Array of custom validation messages.
     *
     * @var Array
     */
    protected $messages = [];

    /**
     * Create a new validation service instance
     * @return void
     */
    public function __construct(Validator $validator){
        $this->validator = $validator;
    }

    /**
     * Set data to validate.
     *
     *
     * @return \MovieApp\Service\Validation\AbstractLaravelValidator
     */
    public function with(array $data){
        $this->data = $data;
        return $this;
    }

    /**
     * Validation passes or fails.
     *
     * @return Boolean
     */
    public function passes(){
        $validator = $this->validator->make(
            $this->data,
            $this->rules,
            $this->messages
        );
        if($validator->fails()){
            $this->errors = $validator->messages();
            return false;
        }
        return true;
    }

    /**
     * Returns errors, if any.
     *
     * @return array
     */
    public function errors(){
        return $this->errors;
    }
}