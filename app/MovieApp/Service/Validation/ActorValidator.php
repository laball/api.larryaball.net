<?php
/**
 * Created by PhpStorm.
 * User: lball
 * Date: 1/18/15
 * Time: 5:13 AM
 */

namespace MovieApp\Service\Validation;

class ActorValidator extends AbstractLaravelValidator{

    /**
     * Validation rules
     *
     * @var Array
     *
     */
    public $rules = array(
        'name' => 'required|unique:actors,name'
    );
    /**
     * Validation messages
     *
     * @var Array
     */
    protected $messages = array(
        'actorname.unique' => 'That actor already exists.'
    );
}