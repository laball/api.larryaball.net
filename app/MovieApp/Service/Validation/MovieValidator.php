<?php
/**
 * Created by PhpStorm.
 * User: lball
 * Date: 1/18/15
 * Time: 5:13 AM
 */

namespace MovieApp\Service\Validation;

class MovieValidator extends AbstractLaravelValidator{

    /**
     * Validation rules
     *
     * @var Array
     *
     */
    public $rules = array(
        'name' => 'required',
        'release_year' => 'required|integer|digits:4|min:1920'
    );
    /**
     * Validation messages
     *
     * @var Array
     */
    protected $messages = array(
        'name.unique' => 'That movie title already exists.'
    );
}