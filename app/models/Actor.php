<?php

class Actor extends \Eloquent {

    protected $table = 'actors';

//    public static $rules = [
//        'name' => 'required'
//    ];

    protected $fillable = array('name', 'image');

    public function Movies(){
        return $this-> belongsToMany('Movie', 'pivot_table');
    }

}