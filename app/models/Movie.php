<?php

class Movie extends \Eloquent {

    protected $table = 'movies';

//    public static $rules = [
//        'name' => 'required',
//        'release_year' => 'required|integer|digits:4|min:1920'
//    ];

    protected $fillable = array('name', 'release_year', 'image');

    public function Actors(){
        return $this->belongsToMany('Actor', 'pivot_table');
    }

}