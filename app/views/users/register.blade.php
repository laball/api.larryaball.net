{{ Form::open(array('url'=>'users/create', 'class'=>'form-signup')) }}
<h2 class="form-signup-heading">Please Register</h2>

{{--<ul>--}}
    {{--@foreach($errors->all() as $error)--}}
        {{--<li>{{ $error }}</li>--}}
    {{--@endforeach--}}
{{--</ul>--}}

{{ Form::text('email', null, array('class'=>'input-block-level', 'placeholder'=>'Email Address')) }}
{{ $errors->first('email', '<p class="error">:message</p>') }}

{{ Form::password('password', array('class'=>'input-block-level', 'placeholder'=>'Password')) }}
{{ $errors->first('password', '<p class="error">:message</p>') }}

{{ Form::password('password_confirmation', array('class'=>'input-block-level', 'placeholder'=>'Confirm Password')) }}
{{ $errors->first('password_confirmation', '<p class="error">:message</p>') }}

{{ Form::submit('Register', array('class'=>'btn btn-large btn-primary btn-block'))}}

{{ HTML::link('users/login', 'Already have an account? Click to login.') }}

{{ Form::close() }}