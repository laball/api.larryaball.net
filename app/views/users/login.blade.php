{{ Form::open(array('url'=>'users/signin', 'class'=>'form-signin')) }}
<h2 class="form-signin-heading">Please Login</h2>

{{ Form::text('email', null, array('class'=>'input-block-level', 'placeholder'=>'Email Address')) }}
{{ $errors->first('email', '<p class="error">:message</p>') }}

{{ Form::password('password', array('class'=>'input-block-level', 'placeholder'=>'Password')) }}
{{ $errors->first('password', '<p class="error">:message</p>') }}

{{ Form::submit('Login', array('class'=>'btn btn-large btn-primary btn-block'))}}
{{ HTML::link('users/register', 'New User? Cick to Register') }}
{{ Form::close() }}