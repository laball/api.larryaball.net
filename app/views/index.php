<!DOCTYPE html>
<html data-ng-app="movieApp">
<head lang="en">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <base href="/"/>
    <title>The Movie App</title>
    <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css"/>
    <link rel="stylesheet" type="text/css" href="css/app.css"/>
    <link rel="stylesheet" type="text/css" href="css/image-crop-styles.css"/>
</head>
<body>
    <nav class="navbar navbar-default" role="navigation">
        <div class="container-fluid">
            <div class="navbar-header">
                <a class="navbar-brand" ui-sref="movies">The Movie App</a>
            </div>
            <div class="collapse navbar-collapse">
                <ul class="nav navbar-nav">
                    <?php if(Auth::check()){ ?>
                    <li><a href="users/logout">Log Out</a></li>
                <?php } ?>
                </ul>
            </div>
        </div>
    </nav>
    <div class="container">
        <?php if(Session::has('message')){
                if(Session::get('message-type') == "info"){ ?>
                <p class="info-message"><?php echo Session::get('message') ?></p>
        <?php }elseif(Session::get('message-type') == "warning"){ ?>
                <p class="alert"><span class="alert-icon"></span><?php echo Session::get('message') ?></p>
        <?php }else{ ?>
                <p class="info-message"></p>
        <?php }
        } ?>
        <div class="row top-buffer">
            <div class="col-xs-8 col-xs-offset-2">
                <div ui-view></div>
            </div>
        </div>
    </div>
    <script type="text/javascript" src="js/vendor/angular.js"></script>
    <script type="text/javascript" src="js/app.js"></script>
    <script type="text/javascript" src="js/controllers.js"></script>
    <script type="text/javascript" src="js/services.js"></script>
    <script type="text/javascript" src="js/directives.js"></script>
    <script type="text/javascript" src="js/filters.js"></script>
    <script type="text/javascript" src="js/vendor/angular-ui-router.js"></script>
    <script type="text/javascript" src="js/vendor/angular-resource.js"></script>
</body>
</html>