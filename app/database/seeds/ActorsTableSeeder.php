<?php

// Composer: "fzaninotto/faker": "v1.3.0"
use Faker\Factory as Faker;

class ActorsTableSeeder extends Seeder {

	public function run()
	{
		$faker = Faker::create();

		foreach(range(1, 25) as $index)
		{
			$actor = Actor::create([
                'name' => $faker->name
			]);
            $min = rand(1, 5);
            $max = rand(6, 10);
            $indices = range($min, $max);
            $actor->Movies()->attach($indices);
		}
	}

}