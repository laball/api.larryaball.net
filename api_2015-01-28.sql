# ************************************************************
# Sequel Pro SQL dump
# Version 4096
#
# http://www.sequelpro.com/
# http://code.google.com/p/sequel-pro/
#
# Host: 127.0.0.1 (MySQL 5.6.22)
# Database: api
# Generation Time: 2015-01-29 02:09:42 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table actors
# ------------------------------------------------------------

DROP TABLE IF EXISTS `actors`;

CREATE TABLE `actors` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `actors` WRITE;
/*!40000 ALTER TABLE `actors` DISABLE KEYS */;

INSERT INTO `actors` (`id`, `name`, `created_at`, `updated_at`)
VALUES
	(26,'Al Pacino','2015-01-16 13:26:30','2015-01-16 13:26:30'),
	(27,'Robert Deniro','2015-01-16 13:30:15','2015-01-16 13:30:15'),
	(28,'Robert Downey Jr.','2015-01-16 13:31:45','2015-01-16 13:31:45'),
	(29,'Marissa Tome','2015-01-16 13:33:16','2015-01-16 13:33:16'),
	(30,'Val Kilmer','2015-01-16 13:36:01','2015-01-16 13:36:01'),
	(31,'Mark Wahlberg','2015-01-16 16:25:31','2015-01-16 16:25:31'),
	(32,'Kevin Spacey','2015-01-16 16:30:59','2015-01-16 16:30:59'),
	(33,'Robert Redford','2015-01-16 16:37:26','2015-01-16 16:37:26'),
	(34,'Gary Busey','2015-01-16 16:42:58','2015-01-16 16:42:58'),
	(35,'Tom Sizemore','2015-01-17 01:51:52','2015-01-17 01:51:52'),
	(36,'Denzel Washington','2015-01-17 06:58:31','2015-01-17 06:58:31'),
	(37,'John Goodman','2015-01-17 07:01:55','2015-01-17 07:01:55'),
	(38,'Kurt Russell','2015-01-17 07:04:37','2015-01-17 07:04:37'),
	(39,'Sam Elliott','2015-01-17 07:14:55','2015-01-17 07:14:55'),
	(40,'Bill Paxton','2015-01-17 07:25:41','2015-01-17 07:25:41'),
	(41,'Billy Bob Thorton','2015-01-17 14:52:35','2015-01-17 14:52:35'),
	(47,'Adam Sandler','2015-01-25 16:25:33','2015-01-25 16:25:33'),
	(48,'Matt Damon','2015-01-25 16:26:12','2015-01-25 16:26:12'),
	(49,'Tom Hanks','2015-01-25 17:16:00','2015-01-25 17:16:00'),
	(50,'Sylvester Stallone','2015-01-27 14:47:35','2015-01-27 14:47:35'),
	(51,'Michelle Pfieffer','2015-01-27 14:50:22','2015-01-27 14:50:22'),
	(52,'Robin Wright','2015-01-27 14:52:04','2015-01-27 14:52:04'),
	(53,'James Gandolfini','2015-01-27 14:55:30','2015-01-27 14:55:30'),
	(54,'Noomi Rapace','2015-01-27 14:55:57','2015-01-27 14:55:57'),
	(55,'Tom Hardy','2015-01-27 14:56:10','2015-01-27 14:56:10'),
	(56,'Christopher MacDonald','2015-01-27 15:04:55','2015-01-27 15:04:55'),
	(57,'Kåre Hedebrant','2015-01-27 15:07:06','2015-01-27 15:07:06'),
	(58,'Ben Stiller','2015-01-27 15:09:42','2015-01-27 15:09:42'),
	(59,'Cameron Diaz','2015-01-27 15:09:51','2015-01-27 15:09:51'),
	(60,'Matthew Modine','2015-01-27 15:12:27','2015-01-27 15:12:27'),
	(61,'R. Lee Ermey','2015-01-27 15:12:51','2015-01-27 15:12:51'),
	(62,'Scott Grimes','2015-01-27 15:24:24','2015-01-27 15:24:24'),
	(63,'Matthew Leitch','2015-01-27 15:24:40','2015-01-27 15:24:40'),
	(64,'Zach Galifianakis','2015-01-27 15:28:26','2015-01-27 15:28:26'),
	(65,'Bradley Cooper','2015-01-27 15:28:50','2015-01-27 15:28:50'),
	(66,'Marlon Brando','2015-01-27 15:31:03','2015-01-27 15:31:03'),
	(67,'Ed Harris','2015-01-27 15:34:49','2015-01-27 15:34:49'),
	(68,'Renée Zellweger','2015-01-27 15:35:01','2015-01-27 15:35:01'),
	(69,'Viggo Mortensen','2015-01-27 15:35:14','2015-01-27 15:35:14');

/*!40000 ALTER TABLE `actors` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table migrations
# ------------------------------------------------------------

DROP TABLE IF EXISTS `migrations`;

CREATE TABLE `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `migrations` WRITE;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;

INSERT INTO `migrations` (`migration`, `batch`)
VALUES
	('2015_01_14_234949_create_users_table',1),
	('2015_01_14_235947_create_movies_table',1),
	('2015_01_15_000132_create_actors_table',1),
	('2015_01_15_001739_create_pivot_table',1);

/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table movies
# ------------------------------------------------------------

DROP TABLE IF EXISTS `movies`;

CREATE TABLE `movies` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `release_year` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `movies` WRITE;
/*!40000 ALTER TABLE `movies` DISABLE KEYS */;

INSERT INTO `movies` (`id`, `name`, `release_year`, `created_at`, `updated_at`)
VALUES
	(1,'Scarface',1978,'2015-01-15 08:29:41','2015-01-27 14:50:24'),
	(18,'Heat',1998,'2015-01-16 23:42:28','2015-01-16 23:42:28'),
	(19,'Tombstone',1993,'2015-01-17 07:05:05','2015-01-17 07:05:05'),
	(20,'Iron Man',2008,'2015-01-17 15:55:52','2015-01-17 15:55:52'),
	(24,'Billy Madison',1995,'2015-01-25 17:14:00','2015-01-25 17:14:00'),
	(25,'Water Boy',1999,'2015-01-25 17:15:19','2015-01-25 17:15:19'),
	(29,'Lone Survivor',2014,'2015-01-27 14:45:14','2015-01-27 14:45:14'),
	(31,'Forrest Gump',1994,'2015-01-27 14:52:08','2015-01-27 14:52:08'),
	(32,'The Drop',2014,'2015-01-27 14:56:14','2015-01-27 14:56:14'),
	(35,'Rocky IV',1985,'2015-01-27 14:59:00','2015-01-27 14:59:00'),
	(36,'Happy Gilmore',1996,'2015-01-27 15:04:59','2015-01-27 15:04:59'),
	(37,'Let The Right One In',2008,'2015-01-27 15:07:12','2015-01-27 15:07:12'),
	(38,'There\'s Something About Mary',1998,'2015-01-27 15:09:59','2015-01-27 15:09:59'),
	(39,'Full Metal Jacket',1987,'2015-01-27 15:12:56','2015-01-27 15:12:56'),
	(40,'Band of Brothers',2001,'2015-01-27 15:24:44','2015-01-27 15:24:44'),
	(41,'The Hangover',2009,'2015-01-27 15:28:53','2015-01-27 15:28:53'),
	(42,'The Godfather',1972,'2015-01-27 15:31:37','2015-01-27 15:31:37'),
	(43,'Apaloosa',2008,'2015-01-27 15:35:46','2015-01-27 15:35:46');

/*!40000 ALTER TABLE `movies` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table pivot_table
# ------------------------------------------------------------

DROP TABLE IF EXISTS `pivot_table`;

CREATE TABLE `pivot_table` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `movie_id` int(11) NOT NULL,
  `actor_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `pivot_table` WRITE;
/*!40000 ALTER TABLE `pivot_table` DISABLE KEYS */;

INSERT INTO `pivot_table` (`id`, `movie_id`, `actor_id`, `created_at`, `updated_at`)
VALUES
	(156,18,26,'0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(157,18,27,'0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(158,18,30,'0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(299,19,38,'0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(300,19,39,'0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(301,19,40,'0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(302,19,41,'0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(305,20,28,'0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(309,24,47,'0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(310,25,47,'0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(326,29,31,'0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(328,1,26,'0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(329,1,51,'0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(330,31,49,'0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(331,31,52,'0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(332,32,53,'0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(333,32,54,'0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(334,32,55,'0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(337,35,50,'0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(338,36,47,'0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(339,36,56,'0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(340,37,57,'0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(341,38,59,'0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(342,38,58,'0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(343,38,48,'0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(344,39,60,'0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(345,39,61,'0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(346,40,62,'0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(347,40,63,'0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(348,41,64,'0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(349,41,65,'0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(350,42,26,'0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(351,42,66,'0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(352,43,67,'0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(353,43,68,'0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(354,43,69,'0000-00-00 00:00:00','0000-00-00 00:00:00');

/*!40000 ALTER TABLE `pivot_table` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table users
# ------------------------------------------------------------

DROP TABLE IF EXISTS `users`;

CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;

INSERT INTO `users` (`id`, `email`, `password`, `remember_token`, `created_at`, `updated_at`)
VALUES
	(1,'Shirley48@Vandervort.info','$2y$10$TboUxgc40NfLTEdDf5.hQeuFCv16zn9W5GYSaBg.BRdHqW3QcBISe',NULL,'2015-01-15 08:29:41','2015-01-15 08:29:41'),
	(2,'Pagac.Jamel@gmail.com','$2y$10$bRLS5AMuRZneUjcck/b4xOPvTU2dT9AGMDfIflPiHSAxw/O9Z1oLS',NULL,'2015-01-15 08:29:41','2015-01-15 08:29:41'),
	(3,'Antonetta88@Harvey.info','$2y$10$TJ/Hb/mgf8I8u2ecLbwQg.gXz5ziAGVH/Vy96SYVDn8UNGeZMmZTi',NULL,'2015-01-15 08:29:41','2015-01-15 08:29:41'),
	(4,'Joaquin48@hotmail.com','$2y$10$VJGNFiZT.tOxXLLIqLwVLuGqhRiF0bl65KoxtZemqqIm2U5pfIpem',NULL,'2015-01-15 08:29:41','2015-01-15 08:29:41'),
	(5,'Sonya.Davis@yahoo.com','$2y$10$WnwoI1i5o5rG0HUnvSJl4OGcuH/KFE3dVB0Q3ZCLSZ4MmvTTNRmZ2',NULL,'2015-01-15 08:29:41','2015-01-15 08:29:41'),
	(6,'larry@larryaball.net','$2y$10$28/nx/OcsJZn9Ww5EHe8KO/IDEgsn3GhG6HzIo80PJi97gikUMrbq','mfMF5g5ixqqXUV4RaegG5TTs5qcw8it2CZ2DmZeA9w0s5TRXbfl3KVU2SQLy','2015-01-26 10:52:27','2015-01-27 14:42:47'),
	(7,'larry.ball7@gmail.com','$2y$10$fO3Yyzfr691qLjkVwguJHeaPu0oDh8.bFY7Bwtygtcjb0FwqWYwie',NULL,'2015-01-27 06:10:16','2015-01-27 06:10:16'),
	(8,'larry@larryaball.com','$2y$10$VTviRiIwUC.5CpNZyZb63OaMdYIFGmT6QCmpVIR/IeByrALXsHT7W',NULL,'2015-01-27 06:12:30','2015-01-27 06:12:30'),
	(9,'larry@mail.com','$2y$10$gelpA8M2/yox4ZF0qXOD/OOQmmInxOWFpe7hqmCXmSW9.HPekyGRy',NULL,'2015-01-27 14:43:32','2015-01-27 14:43:32');

/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;



/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
